module.exports = {
  mailer: {
    options: {
      from: 'example@gmail.com',
      to: 'example@gmail.com',
      subject: 'Hello, new offer request is here 🎉!',
    },
    smtpUrl: 'smtps://example%40gmail.com:examplepass@smtp.gmail.com',
  },
};
