const config = require('../config.js');
const emails = require('../lib/emails.js');
const mailer = require('nodemailer');

const transporter = mailer.createTransport(config.mailer.smtpUrl);

module.exports = (req, res) => {
  if (!emails.validateEmail(req.body.email) || !req.body.message) {
    res.status(400).send({ code: 400, message: 'Please provide valid email addesss and message.' });
    return;
  }

  const mailOptions = Object.assign(config.mailer.options, {
    html: emails.emailTemplate(req.body.email, req.body.message),
  });

  transporter.sendMail(mailOptions, (error) => {
    if (error) {
      console.error(error); // eslint-disable-line
      res.status(500).send({ code: 500, message: 'There was an error sending your email.' });
      return;
    }

    res.status(200).send({
      code: 200,
      message: 'Email sent. Thanks for contacting us. We\'ll reply soon.',
    });
  });
};
