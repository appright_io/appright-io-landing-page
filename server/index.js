const express = require('express');
const logger = require('./lib/logger');
const bodyParser = require('body-parser');
const contact = require('./middlewares/contact');
const frontend = require('./middlewares/frontend');

const isDev = process.env.NODE_ENV !== 'production';

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.post('/contact', contact);

const webpackConfig = isDev
  ? require('../internals/webpack/webpack.dev')
  : require('../internals/webpack/options');

app.use(frontend(webpackConfig));

const port = process.env.PORT || 8000;

app.listen(port, (err) => {
  if (err) {
    return logger.onError(err);
  }

  logger.onAppStarted(port);

  return null;
});
