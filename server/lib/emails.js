/* eslint-disable */

exports.validateEmail = (email) => {
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(email);
};

exports.emailTemplate =
  (email, text) =>
    `<div style="background-color:#1d1726;color:white;text-align:center;height:auto;padding:40px 0;">

      <h1 style="margin:20px 0;">👾 Appright</h1>

      <div style="font-size:20px;margin-bottom:20px;color:white!important;">${email} just wrote:</div>

      <div>${text}</div>

      <div style="height:auto;margin:30px 0;">
        <a style="background-color:white;color:#1d1726;padding:10px 20px;text-decoration:none;border-radius:10px;"href="mailto:${email}?Subject=Hello!%20thanks%20for%20contacting%20us" target="_top">Reply now!</a>
      </div>
    </div>`;
