/**
 * COMMON WEBPACK CONFIGURATION
 */

const webpack = require('webpack');
const opts = require('./options');

module.exports = (options) => ({
  entry: options.entry,
  output: Object.assign({}, options.output, opts.output), // Merge with env dependent settings
  module: {
    loaders: [{
      test: /\.(jsx|js)$/, // Transform all .js(x) files required somewhere with Babel
      loader: 'babel',
      exclude: /node_modules/,
      query: options.babelQuery,
    }, {
      // Transform our own .css files with PostCSS and CSS-modules
      test: /\.css$/,
      exclude: /(node_modules|assets)/,
      loader: options.cssLoaders,
    }, {
      // Do not transform vendor's CSS with CSS-modules
      // The point is that they remain in global scope.
      // Since we require these CSS files in our JS or CSS files,
      // they will be a part of our compilation either way.
      // So, no need for ExtractTextPlugin here.
      test: /\.css$/,
      include: /(node_modules|assets)/,
      loader: options.vendorCssLoaders,
    }, {
      test: /\.html$/,
      loader: 'html-loader',
    }, {
      test: /\.(ttf|eot|woff|woff2|svg)$/,
      loader: 'url?limit=10000',
    }, {
      test: /.*\.(gif|png|jpe?g)$/i,
      loaders: [
        'file?hash=sha512&digest=hex&name=[hash].[ext]',
        'image-webpack?{progressive:true, '
        + 'optimizationLevel: 7, interlaced: false, pngquant:{quality: "65-90", speed: 4}}',
      ],
    }],
  },
  plugins: options.plugins.concat([
    new webpack.optimize.CommonsChunkPlugin('common.js'),
    new webpack.ProvidePlugin({
      fetch: 'imports?this=>global!exports?global.fetch!whatwg-fetch',
    }),
  ]),
  postcss: () => options.postcssPlugins,
  resolve: {
    modulesDirectories: [
      'components',
      'actions',
      'containers',
      'selectors',
      'reducers',
      'sagas',
      'models',
      'assets',
      'node_modules',
    ],
    extensions: [
      '',
      '.js',
      '.jsx',
      '.react.js',
      '.css',
    ],
  },
  devtool: options.devtool,
  target: 'web', // Make web variables accessible to webpack, e.g. window
  stats: false, // Don't show stats in the console
  progress: true,
});
