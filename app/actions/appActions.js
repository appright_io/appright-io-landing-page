import { createRequestTypes, requestAction, action } from './helpers';

/**
 * Constants
 */
export const APPLICATION_MESSAGE = 'APPLICATION_MESSAGE';
export const DISMISS_APPLICATION_MESSAGE = 'DISMISS_APPLICATION_MESSAGE';
export const CHANGE_THUMBNAIL = 'CHANGE_THUMBNAIL';
export const UPDATE_CONTACT_FORM = 'UPDATE_CONTACT_FORM';
export const SEND_EMAIL = createRequestTypes('SEND_EMAIL');
export const TOGGLE_MENU = 'TOGGLE_MENU';

/**
 * Actions
 */
export const applicationMessage = (code, message) => action(APPLICATION_MESSAGE, { code, message });
export const dismissError = () => action(DISMISS_APPLICATION_MESSAGE);
export const changeThumbnail = (activeIndex) => action(CHANGE_THUMBNAIL, activeIndex);
export const updateFormField = (name, value) => action(UPDATE_CONTACT_FORM, { name, value });
export const sendEmail = requestAction(SEND_EMAIL, ['email', 'message']);
export const toggleMenu = (isOpen) => action(TOGGLE_MENU, { isOpen });
