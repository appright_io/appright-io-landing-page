import React from 'react';
import { connect } from 'react-redux';
import { PureComponent } from 'react-pure-render';

import ApplicationBox from 'ApplicationBox';
import PageMeta from 'PageMeta';
import Background from 'Background';
import HeadingPage from 'HeadingPage';
import AboutUs from 'AboutUs';
import Products from 'Products';
import Workflow from 'Workflow';
import Technology from 'Technology';
import Contact from 'Contact';
import Navbar from 'Navbar';

import { dismissError, changeThumbnail, toggleMenu } from 'appActions';

import {
  applicationResponseSelector,
  activeIndexSelector,
  contactFormSelector,
  isMenuOpenSelector,
  menuSelector,
} from 'appSelectors';

/**
 * Home page
 */
class Home extends PureComponent {
  static propTypes = {
    dispatch: React.PropTypes.func.isRequired,
    response: React.PropTypes.object,
    activeIndex: React.PropTypes.number,
    isMenuOpen: React.PropTypes.bool,
    menu: React.PropTypes.array,
  };

  render() {
    const { dispatch, response, activeIndex, isMenuOpen, menu } = this.props;

    const isMenuOpenFunc = ({ isOpen }) => {
      dispatch(toggleMenu(isOpen));
      return isOpen;
    };

    return (
      <PageMeta title="Welcome">
        <Background className={isMenuOpen ? 'hide-burger' : 'show-burger'}>
        <ApplicationBox response={response} onDismiss={() => dispatch(dismissError())} />
        <Navbar
          isOpen={isMenuOpen}
          isMenuOpen={isMenuOpenFunc}
          closeMenu={() => dispatch(toggleMenu(false))}
          itemList={menu}
        />
        <HeadingPage />
        <AboutUs />
        <Products />
        <Workflow />
        <Technology
          activeIndex={activeIndex}
          onThumbClick={(number) => dispatch(changeThumbnail(number))}
        />
        <Contact />
        </Background>
    </PageMeta>
    );
  }

}

export default connect(
  (state) => ({
    response: applicationResponseSelector(state),
    activeIndex: activeIndexSelector(state),
    contactForm: contactFormSelector(state),
    isMenuOpen: isMenuOpenSelector(state),
    menu: menuSelector(),
  })
)(Home);
