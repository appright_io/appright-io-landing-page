import { Record, Map } from 'immutable';
import {
  APPLICATION_MESSAGE,
  DISMISS_APPLICATION_MESSAGE,
  CHANGE_THUMBNAIL,
  SEND_EMAIL,
  UPDATE_CONTACT_FORM,
  TOGGLE_MENU,
} from 'appActions';
import * as form from '../lib/forms';

const ContactForm = Record({
  errors: Map({}),
  pending: false,
  fields: new (Record({
    email: '',
    message: '',
  })),
});

const initialState = new (Record({
  response: null,
  activeIndex: 1,
  contactForm: new ContactForm,
  isMenuOpen: false,
}));

export default function userStore(state = initialState, action) {
  if (!(state instanceof Record)) return initialState.merge(state);

  switch (action.type) {

    case APPLICATION_MESSAGE:
      return state.set('response', action.payload);

    case CHANGE_THUMBNAIL:
      return state.set('activeIndex', action.payload);

    case DISMISS_APPLICATION_MESSAGE:
      return state.remove('response');

    case TOGGLE_MENU:
      return state.set('isMenuOpen', action.payload.isOpen);

    case UPDATE_CONTACT_FORM: {
      const { name, value } = action.payload;
      return form.updateFormField(state, 'contactForm', {
        value: name === 'email' ? value.toLowerCase() : value,
        name,
      });
    }

    case SEND_EMAIL.REQUEST:
      return form.markFormPending(state, 'contactForm');

    case SEND_EMAIL.SUCCESS: {
      return state.set('contactForm', new ContactForm);
    }

    case SEND_EMAIL.FAILURE: {
      const errors = action.payload;
      return form.updateFormError(state, 'contactForm', errors);
    }

  }

  return state;
}
