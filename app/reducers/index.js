import { combineReducers } from 'redux-immutable';

import app from './appReducer';

export default combineReducers({
  app,
});
