// Import Babel Core
import 'babel-polyfill';
import Sniffr from 'sniffr';

require('es6-promise').polyfill();

// Setup React
import React from 'react';
import ReactDOM from 'react-dom';
import store from './store';
import { Provider } from 'react-redux';
import fastclick from 'fastclick';

// Load stylesheets
import '../assets/style/index.css';
import '../assets/fonts/index.css';

const rootEl = document.getElementById('root');

const systemInformation = new Sniffr();
systemInformation.sniff(window.navigator.userAgent);

if (systemInformation.browser.name === 'edge') {
  document.body.addEventListener('wheel', (event) => {
    event.preventDefault();
    window.scrollTo(0, window.pageYOffset - event.wheelDelta);
  });
} else if (systemInformation.browser.name === 'ie') {
  //eslint-disable-next-line
  window.alert('Sorry 😖! We decided to drop support for IE11, please upgrade your browser to Microsoft Edge, or Google Chrome 🎉.');
}

// Export global variables
global.systemInformation = systemInformation;
global.animator = require('react-spark-scroll-rekapi')({ invalidateAutomatically: true });

fastclick.attach(document.body);

const renderApp = () => {
  const router = require('./router').default;

  ReactDOM.render(
    <Provider store={store}>
      {router}
    </Provider>,
    rootEl
  );
};

const renderError = (error) => {
  const RedBox = require('redbox-react');

  ReactDOM.render(
    <RedBox error={error} />,
    rootEl
  );
};

let render;

if (module.hot) {
  render = () => {
    try {
      renderApp();
    } catch (error) {
      renderError(error);
    }
  };
  module.hot.accept('./router', () => {
    setTimeout(render);
  });
} else {
  render = renderApp;
}

render();
