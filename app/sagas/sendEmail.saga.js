import { SEND_EMAIL, sendEmail, applicationMessage } from 'appActions';
import { take, call, put } from 'redux-saga/effects';
import { fetch } from '../api';

export default function* sendEmailSaga() {
  while (true) {
    const { payload: { email, message } } = yield take(SEND_EMAIL.REQUEST);

    try {
      const response = yield call(fetch, 'contact', {
        method: 'POST',
        body: { email, message },
      });

      yield put(sendEmail.success(response));
      yield put(applicationMessage(response.code, response.message));
    } catch (err) {
      yield put(sendEmail.failure(err));
      yield put(applicationMessage(err.statusCode));
    }
  }
}
