import React from 'react';
import { PureComponent } from 'react-pure-render';
import { Element } from 'react-scroll';
import { connect } from 'react-redux';
import cx from 'classnames';

import Section from 'Section';
import Headline from 'Headline';
import ContactForm from 'ContactForm';
import SocialMedia from 'SocialMedia';
import Footer from 'Footer';
import Animate from 'Animate';

import styles from './styles.css';

import { updateFormField, sendEmail } from 'appActions';

export default class Contact extends PureComponent {

  static propTypes = {
    contactForm: React.PropTypes.object.isRequired,
    dispatch: React.PropTypes.func.isRequired,
  };

  render() {
    const { dispatch, contactForm } = this.props;

    const submitForm = () => {
      const { email, message } = contactForm.fields;
      return dispatch(sendEmail.request(email, message));
    };

    const onChangeText = ({ target: { name, value } }) =>
      dispatch(updateFormField(name, value));

    return (
      <Section className="flex flex-column justify-between" type="white">
        <Element id="contact" name="contact">
          <Animate
            className="flex flex-column items-center pt3 element"
            timeline={{ topBottom: { opacity: 0 }, centerCenter: { opacity: 1 } }}
          >
            <Headline className="my3" index={5} text="Contact" />

            <span className={cx('mb1', styles.light)}>Contact us via our simple form</span>

            <ContactForm
              form={contactForm}
              onChangeText={onChangeText}
              onFormSubmit={submitForm}
            />

            <span className={cx('my1', styles.light)}>Or if you prefer traditional form</span>

            <div className="flex flex-column items-center my1">
              <strong className="underline">info@appright.io</strong>
              <strong className="underline my1">666 999 888</strong>

              <SocialMedia facebook twitter instagram snapchat />
            </div>

            <Footer
              className="my1 flex flex-column items-center"
              year={2016}
              company="appright"
            />

          </Animate>
        </Element>
      </Section>
    );
  }
}

export default connect(
  (state) => ({
    contactForm: state.get('app').contactForm,
  })
)(Contact);
