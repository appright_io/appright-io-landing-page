import React from 'react';
import cx from 'classnames';

import Icon from 'Icon';

import styles from './styles.css';

export default function Work({
    className,
    position = 'left',
    image,
    faIconId,
    title,
    children,
    gradient,
  }) {
  return (
    <div className={cx('px2 flex flex-wrap', className, styles[position])} >
      <div
        className={
          cx('flex flex-column justify-center sm-center p3',
          styles.text)
        }
      >
        <Icon faIconId={faIconId} />
        <h4 className="py2">{title}</h4>
        {children}
      </div>
      <div className={cx('flex justify-center', styles.imageContainer)} >
        <div
          style={{ backgroundImage: `url(${image})` }}
          className={cx(styles.image, gradient ? styles[gradient] : void 0)}
        />
      </div>
    </div>
  );
}

Work.propTypes = {
  className: React.PropTypes.any,
  position: React.PropTypes.string,
  image: React.PropTypes.any.isRequired,
  faIconId: React.PropTypes.string.isRequired,
  title: React.PropTypes.string.isRequired,
  children: React.PropTypes.any,
  gradient: React.PropTypes.string,
};
