import React from 'react';
import cx from 'classnames';

import styles from './styles.css';

export default function WorkDivider() {
  return (
        <div className="col-8 sm-col-8 md-col-12 center mx-auto">
          <hr className={cx('col-12', styles.line)} />
        </div>
  );
}

WorkDivider.propTypes = {
  position: React.PropTypes.any,
};
