import React from 'react';
import cx from 'classnames';

import { TopPolygon, BottomPolygon } from 'Polygon';

import styles from './styles.css';

export default function Section(
  { id, children, className, type, topPolygon = false, bottomPolygon = false }
) {
  const isTransparentOrBlack = type === 'transparent' || 'black';

  return (
    <section id={id} className={cx(className, styles.base, styles[type])}>
      { isTransparentOrBlack && topPolygon && (<TopPolygon />) }
      { children }
      { isTransparentOrBlack && bottomPolygon && (<BottomPolygon />) }
    </section>
  );
}

Section.propTypes = {
  id: React.PropTypes.string,
  children: React.PropTypes.any,
  className: React.PropTypes.string,
  type: React.PropTypes.string,
  topPolygon: React.PropTypes.bool,
  bottomPolygon: React.PropTypes.bool,
};
