import React from 'react';
import { PureComponent } from 'react-pure-render';
import { Element } from 'react-scroll';

import Section from 'Section';
import Headline from 'Headline';
import Thumbnail from 'Thumbnail';
import TechnologySection from 'TechnologySection';

export default class Technology extends PureComponent {

  static propTypes = {
    activeIndex: React.PropTypes.number.isRequired,
    onThumbClick: React.PropTypes.func.isRequired,
  };

  render() {
    const { activeIndex, onThumbClick } = this.props;

    const isActiveIndex = (index) => activeIndex === index;

    return (
      <Section
        className="flex flex-column justify-between"
        type="black"
        topPolygon
        bottomPolygon
      >
        <Element
          id="technology"
          name="technology"
          className="flex flex-column items-center pt3 element"
        >

          <Headline className="my3" index={4} text="Technology" invert />

          <div
            className="flex flex-wrap flex-row flex-auto md-justify-between
                      sm-col col-12 sm-col-10 md-col-10 lg-col-6 my2"
          >
            <Thumbnail
              faIconId="fa-desktop"
              active={isActiveIndex(1)}
              onClick={() => { onThumbClick(1); }}
              clickable
            />
            <Thumbnail
              faIconId="fa-gears"
              active={isActiveIndex(2)}
              onClick={() => { onThumbClick(2); }}
              clickable
            />
            <Thumbnail
              faIconId="fa-mobile"
              active={isActiveIndex(3)}
              onClick={() => { onThumbClick(3); }}
              clickable
            />
            <Thumbnail
              faIconId="fa-ellipsis-h"
              active={isActiveIndex(4)}
              onClick={() => { onThumbClick(4); }}
              clickable
            />
          </div>

          <div className="flex flex-row justify-center col-12 sm-col-12 lg-col-10 my2">
              {isActiveIndex(1) && (
                <TechnologySection className="col-10 p2" index={1}>
                  Do you want to have cool and reusable web application which
                  is extremely fast and fun to use? Or maybe just simple web
                  page with stunning design which could present your business
                  as innovative and professional company? Let us do the job.
                  With our powerful tools and skilled developers we can create
                  React-ive web applications. Easy to mantain.
                  All based on JavaScript.
                </TechnologySection>
              )}
              {isActiveIndex(2) && (
                <TechnologySection className="col-10 p2" index={2}>
                  Async is cool. Node is even better on backend side.
                  Trust us, our developers knows exactly how to create great
                  real-time APIs. We have experience in writing great  HTTP
                  based applications, so if you need fast, bug-free application
                  - then JavaScript is the choice. appright is the choice.
                </TechnologySection>
              )}
              {isActiveIndex(3) && (
                <TechnologySection className="col-10 p2" index={3}>
                  Time of creating boring application for each platform,
                  which took alot of our time and your money are gone.
                  Let us bring you top-class React Native application.
                  It’s better, faster, cooler, cheaper. Don’t let others
                  be first. Build your application with us, save your time
                  and money and bring highest quality products for your
                  client. It’s React Native. It’s future.
                </TechnologySection>
              )}
              {isActiveIndex(4) && (
                <TechnologySection className="col-10 p2" index={4}>
                  We are full-stack agency. Our developers can do almost
                  everything - if we don’t know something - we’ll learn
                  it super fast. Bring your ideas to our company. We’ll
                  help you materialize all your ideas. Doesn’t matter if
                  its desktop application or drone flying around the corner.
                  Just write to us. We’ll help you.
                </TechnologySection>
              )}
          </div>
        </Element>
      </Section>
    );
  }
}
