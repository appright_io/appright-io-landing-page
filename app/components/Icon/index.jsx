import React from 'react';
import cx from 'classnames';

export default function Icon({ className, faIconId }) {
  return (<i className={cx(`fa ${faIconId}`, className)}></i>);
}

Icon.propTypes = {
  className: React.PropTypes.any,
  faIconId: React.PropTypes.string.isRequired,
};
