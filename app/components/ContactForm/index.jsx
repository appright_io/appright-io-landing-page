import React from 'react';
import { PureComponent } from 'react-pure-render';
import cx from 'classnames';

import Button from 'Button';

import styles from './styles.css';

export default class ContactForm extends PureComponent {

  static propTypes = {
    form: React.PropTypes.object.isRequired,
    onChangeText: React.PropTypes.func.isRequired,
    onFormSubmit: React.PropTypes.func.isRequired,
  };

  preventDefault(func) {
    return (e) => {
      e.preventDefault();
      if (typeof func === 'function') {
        func(e);
      }
    };
  }

  render() {
    const {
      form: { fields, errors, pending },
      onFormSubmit,
      onChangeText,
    } = this.props;

    const formErrors = errors.toJS();

    return (
      <form
        className="mb3 col-10 sm-col-10 md-col-8 lg-col-4 flex flex-column"
        disabled={pending}
        onSubmit={this.preventDefault(onFormSubmit)}
      >
        <input
          className={cx('col-12 mt1 py1 px2', styles.input)}
          name="email"
          onChange={onChangeText}
          placeholder="john@doe.com"
          spellCheck={false}
          type="email"
          value={fields.email}
          error={formErrors.email}
          required
        />

        <textarea
          className={cx('col-12 mt1 py1 px2', styles.area)}
          placeholder="Describe your project"
          rows={5}
          name="message"
          onChange={onChangeText}
          spellCheck={false}
          type="text"
          value={fields.message}
          error={formErrors.message}
          required
        />

        <Button className="mt2" text="Send message" type="submit" inverse />
      </form>
    );
  }
}
