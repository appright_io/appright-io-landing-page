import React from 'react';
import DocumentTitle from 'react-document-title';

const capitalize = (word) => {
  const letters = word.split('');
  letters[0] = letters[0].toUpperCase();
  return letters.join('');
};

export default function PageMeta({ title, children }) {
  const metaTitle = title ? `${capitalize(title)} | appright` : 'appright';

  return (
    <DocumentTitle title={metaTitle}>
      {children}
    </DocumentTitle>
  );
}

PageMeta.propTypes = {
  title: React.PropTypes.string.isRequired,
  children: React.PropTypes.any,
};
