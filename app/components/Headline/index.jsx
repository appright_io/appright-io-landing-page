import React from 'react';
import cx from 'classnames';

import Animate from 'Animate';

import styles from './styles.css';

export default function Headline({ className, index, text, invert = false }) {
  const addZero = (number) => number < 10 ? `0${number}.` : number;

  return (
    <div className={cx('flex flex-column', className, invert ? styles.invert : styles.base)}>
      <Animate timeline={{
        topBottom: { opacity: 0.5 },
        centerCenter: { opacity: 1 },
      }}
      >
        <span>{addZero(index)}</span>
        <hr />
      </Animate>
      <Animate timeline={{
        topBottom: { marginLeft: '200px', opacity: 0.5 },
        centerCenter: { marginLeft: '0px', opacity: 1 },
      }}
      >
        <h2 className="upper">{text}</h2>
      </Animate>
    </div>);
}

Headline.propTypes = {
  className: React.PropTypes.any,
  index: React.PropTypes.number.isRequired,
  text: React.PropTypes.string.isRequired,
  invert: React.PropTypes.bool,
};
