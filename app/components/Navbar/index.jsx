import React from 'react';
import { PureComponent } from 'react-pure-render';
import cx from 'classnames';
import { Link } from 'react-scroll';
import { stack as Menu } from 'react-burger-menu';

import styles from './styles.css';

const isFirefox = navigator.userAgent.toLowerCase().indexOf('firefox') > -1;

export default class Navbar extends PureComponent {
  static propTypes = {
    className: React.PropTypes.any,
    itemList: React.PropTypes.array,
    isOpen: React.PropTypes.bool,
    isMenuOpen: React.PropTypes.func,
    closeMenu: React.PropTypes.func,
  };

  render() {
    const { itemList, closeMenu, isMenuOpen, isOpen } = this.props;

    const items = (listedItems) =>
      listedItems.map((item, index) =>
        isFirefox ?
        (<a
          activeClass="active"
          className={cx('px2 py1', styles.menuItem)}
          href={`#${item.id}`}
          spy
          smooth
          duration={1000}
          key={item.id}
          onClick={() => closeMenu()}
        >
          <div className="flex flex-row items-end">
            <span className="p1">{`${index + 1}.`}</span>
            <aside>{item.content}</aside>
          </div>
        </a>) :
        (<Link
          activeClass="active"
          className={cx('px2 py1', styles.menuItem)}
          to={item.id}
          spy
          smooth
          duration={1000}
          key={item.id}
          onClick={() => closeMenu()}
        >
          <div className="flex flex-row items-end">
            <span className="p1">{`${index + 1}.`}</span>
            <aside>{item.content}</aside>
          </div>
        </Link>)
      );

    return (
      <Menu
        noOverlay
        isOpen={isOpen}
        onStateChange={isMenuOpen}
        right
        width={250}
      >
        {items(itemList)}
      </Menu>);
  }
}
