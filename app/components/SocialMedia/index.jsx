import React from 'react';
import cx from 'classnames';

import Icon from 'Icon';

export default function SocialMedia({
  className,
  facebook = false,
  twitter = false,
  instagram = false,
  snapchat = false,
}) {
  return (
    <div className={cx('flex flex-row justify-between col-12', className)}>
      {facebook && (<Icon faIconId="fa-facebook" />)}
      {twitter && (<Icon faIconId="fa-twitter" />)}
      {instagram && (<Icon faIconId="fa-instagram" />)}
      {snapchat && (<Icon faIconId="fa-apple" />)}
    </div>
  );
}

SocialMedia.propTypes = {
  className: React.PropTypes.any,
  facebook: React.PropTypes.bool,
  twitter: React.PropTypes.bool,
  instagram: React.PropTypes.bool,
  snapchat: React.PropTypes.bool,
};
