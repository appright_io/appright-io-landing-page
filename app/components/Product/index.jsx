import React from 'react';
import cx from 'classnames';

import Animate from 'Animate';

import styles from './styles.css';

export default function Product({ children, className, title }) {
  return (
    <Animate
      className={cx(className, styles.base)}
      proxy="scale-proxy"
      timeline={{
        topBottom: { scale: 0.7 },
        centerCenter: { scale: 1, ease: 'easeOutCubic' },
      }}
    >
      <h5 className="upper" >{title}</h5>
      <p className="my1">{children}</p>
    </Animate>
  );
}

Product.propTypes = {
  children: React.PropTypes.string.isRequired,
  className: React.PropTypes.any,
  title: React.PropTypes.string.isRequired,
  author: React.PropTypes.string.isRequired,
  company: React.PropTypes.string,
};
