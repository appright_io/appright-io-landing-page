import React from 'react';
import cx from 'classnames';

import styles from './styles.css';

export default function BackgroundImage({ isPulsing, image }) {
  return (
    <div className={cx(styles.base, isPulsing && styles.pulse)}>
      <img src={image} alt="background-img" />
    </div>
  );
}

BackgroundImage.propTypes = {
  isPulsing: React.PropTypes.bool,
  image: React.PropTypes.any.isRequired,
};
