import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import cx from 'classnames';

import animations from './animations.css';
import styles from './styles.css';

export default function TechnologySection({ className, children, index }) {
  return (
    <ReactCSSTransitionGroup
      className={className}
      transitionAppear
      transitionAppearTimeout={500}
      transitionName={animations}
    >
      <p className={cx('sm-center', styles.base)} key={index}>
        {children}
      </p>
    </ReactCSSTransitionGroup>);
}

TechnologySection.propTypes = {
  className: React.PropTypes.any,
  children: React.PropTypes.any,
  index: React.PropTypes.any.isRequired,
};
