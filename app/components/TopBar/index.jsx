import React from 'react';
import cx from 'classnames';

import styles from './style';

export default function TopBar({ children, right, left, type = 'gray', upper, shadow }) {
  const defaultStyle = 'flex px2 py1 flex-row justify-between';

  const className = cx(defaultStyle, styles.container, styles[`type_${type}`], {
    [styles.withShadow]: shadow,
    [styles.upper]: upper,
  });

  return (
    <div className={className}>
      <div className={cx('col-3', styles.leftColumn)}>
        {left}
      </div>
      <div className={styles.centerColumn}>
        {children}
      </div>
      <div className={cx('col-3', styles.rightColumn)}>
        {right}
      </div>
    </div>
  );
}

TopBar.propTypes = {
  children: React.PropTypes.any,
  right: React.PropTypes.any,
  left: React.PropTypes.any,
  type: React.PropTypes.string,
  shadow: React.PropTypes.bool,
  upper: React.PropTypes.bool,
};
