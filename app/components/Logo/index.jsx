import React from 'react';
import cx from 'classnames';

import styles from './styles.css';

export default function Logo({ className }) {
  return (
    <div className={cx('flex flex-row', className, styles.logo)}>
      <img src={require('img/logo.png')} />
      <h1>appright.io</h1>
    </div>);
}

Logo.propTypes = {
  className: React.PropTypes.any,
};
