import React from 'react';
import { PureComponent } from 'react-pure-render';
import cx from 'classnames';

import Section from 'Section';
import Logo from 'Logo';
import Animate from 'Animate';

import styles from './styles.css';

export default class HeadingPage extends PureComponent {
  render() {
    return (
      <Section className="flex flex-column justify-between" type="transparent" bottomPolygon>
        <div className="flex flex-column flex-auto">
          <div className={cx('flex flex-row mt3', styles.logoWrapper)}>
            <Logo className="sm-col sm-col-12 md-col-4 lg-col-2 left-align sm-center" />
          </div>

          <Animate
            className="flex flex-row flex-auto items-center justify-around"
            timeline={{
              bottomTop: { opacity: 0 },
              centerCenter: { opacity: 1 },
            }}
          />
        </div>
      </Section>
    );
  }
}
