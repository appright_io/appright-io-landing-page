import React from 'react';
import TransitionGroup from 'react-addons-css-transition-group';

import TopBar from 'TopBar';

import styles from './style.css';
import animation from './animation.css';

export default function ApplicationBox({ onDismiss, response }) {
  let msg;
  let okStatus;

  if (response) {
    okStatus = typeof response.code !== 'undefined' && response.code < 300;
    msg = typeof response.message !== 'undefined' ?
    response.message : 'Operation failed. Try again or contact help@appright.io';
  }

  const dismissIcon = (
    <a className="px2" onClick={onDismiss}>X</a>
  );

  return (
    <TransitionGroup
      transitionEnterTimeout={0}
      transitionLeaveTimeout={0}
      transitionName={animation}
    >
      {msg && (
        <div className={styles.container}>
          <TopBar right={dismissIcon} type={okStatus ? 'green' : 'red'} withShadow>{msg}</TopBar>
        </div>
      )}
    </TransitionGroup>
  );
}

ApplicationBox.propTypes = {
  response: React.PropTypes.object,
  onDismiss: React.PropTypes.func.isRequired,
};
