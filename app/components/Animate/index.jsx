import React from 'react';

export default function Animate({ children, className, timeline, proxy }) {
  const { SparkScroll } = global.animator;
  const { browser, os } = global.systemInformation;
  const isDesktop = ['windows', 'macos', 'linux'].includes(os.name);

  const animation = (opacityOnly) =>
    opacityOnly ?
    (
      <SparkScroll.div
        className={className}
        timeline={
        {
          topBottom: { opacity: 0.2 },
          centerCenter: { opacity: 1 },
        }
        }
        proxy={proxy}
      >
        {children}
      </SparkScroll.div>
    ) :
    (
      <SparkScroll.div
        className={className}
        timeline={timeline}
        proxy={proxy}
      >
        {children}
      </SparkScroll.div>
  );

  // Handle animations between browsers to keep it as smooth as possible
  const browserHandler = {
    chrome: animation(false),
    safari: animation(true),
    firefox: animation(true),
    edge: animation(true),
    default: (
      <div className={className}>
        {children}
      </div>
    ),
  };

  return Object.keys(browserHandler).includes(browser.name) && isDesktop ?
    browserHandler[browser.name] : browserHandler.default;
}

Animate.propTypes = {
  children: React.PropTypes.any,
  className: React.PropTypes.any,
  timeline: React.PropTypes.object.isRequired,
  proxy: React.PropTypes.string,
};
