import React from 'react';
import cx from 'classnames';

import Icon from 'Icon';

import styles from './styles.css';

export default function Thumbnail(
  { className, faIconId, text, onClick, active = false, clickable = false }
) {
  return (
    <div
      onClick={onClick}
      className={cx('mx-auto flex-column', className, styles.base)}
    >
      <Icon
        className={cx(active ? styles.active : styles.icon, clickable ? styles.clickable : void 0)}
        faIconId={faIconId}
      />
      {text && (<h2 className="mt1">{text}</h2>)}
    </div>);
}

Thumbnail.propTypes = {
  className: React.PropTypes.any,
  onClick: React.PropTypes.func,
  faIconId: React.PropTypes.string.isRequired,
  text: React.PropTypes.string,
  active: React.PropTypes.bool,
  clickable: React.PropTypes.bool,
};
