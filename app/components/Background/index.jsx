import React from 'react';
import BackgroundImage from 'BackgroundImage';

export default function Background({ className, children }) {
  return (
    <div className={className}>
      <BackgroundImage image={require('img/bg.jpg')} />
      <BackgroundImage isPulsing image={require('img/flying-bg.png')} />
      {children}
    </div>
  );
}

Background.propTypes = {
  children: React.PropTypes.any,
  className: React.PropTypes.string,
};
