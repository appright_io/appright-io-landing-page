import React from 'react';
import cx from 'classnames';

import styles from './styles.css';

export default function Footer({ className, year, company }) {
  return (
    <footer className={cx(className, styles.base)}>
      <hr className="my1" />
      <aside>{`© ${year} ${company} All rights reserved`}</aside>
    </footer>);
}

Footer.propTypes = {
  className: React.PropTypes.any,
  children: React.PropTypes.any,
  year: React.PropTypes.number.isRequired,
  company: React.PropTypes.string.isRequired,
};
