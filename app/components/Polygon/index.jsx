import React from 'react';
import cx from 'classnames';

import styles from './styles.css';

function Polygon({ className }) {
  return (
    <div className={className}>
      <img className={cx('col-8', styles.polygon)} src={require('img/left-polygon.svg')} />
      <img className={cx('col-4', styles.polygon)} src={require('img/right-polygon.svg')} />
    </div>);
}

Polygon.propTypes = {
  className: React.PropTypes.string,
};

export function BottomPolygon() {
  return (
    <Polygon className={styles.base} />
  );
}

export function TopPolygon() {
  return (
    <div className={styles.perspective}>
      <Polygon className={styles.flipped} />
    </div>
  );
}
