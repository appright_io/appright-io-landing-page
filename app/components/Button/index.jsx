import React from 'react';
import cx from 'classnames';

import styles from './styles.css';

export default function Button({ className, text, type = 'button', onClick, inverse = false }) {
  return (
    <button
      onClick={onClick}
      className={cx('py1 px3', className, inverse ? styles.inverse : styles.normal)}
      type={type}
    >
      {text}
    </button>);
}

Button.propTypes = {
  className: React.PropTypes.any,
  text: React.PropTypes.string.isRequired,
  type: React.PropTypes.string,
  onClick: React.PropTypes.func,
  inverse: React.PropTypes.bool,
};
