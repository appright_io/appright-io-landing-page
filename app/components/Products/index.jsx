import React from 'react';
import { PureComponent } from 'react-pure-render';
import { Element } from 'react-scroll';

import Section from 'Section';
import Headline from 'Headline';
import Product from 'Product';

// import styles from './styles.css';

export default class Products extends PureComponent {

  render() {
    return (
      <Section
        className="flex flex-column justify-between"
        type="black"
        topPolygon
        bottomPolygon
      >
        <Element
          id="products"
          name="products"
          className="flex flex-column flex-auto items-center pt3 element"
        >

          <Headline className="my3" index={2} text="Products" invert />

          <div className="flex flex-row flex-auto flex-wrap col-10 my2">
            <Product
              className="sm-col-10 md-col-6 mx-auto p2"
              title="Business consultations"
            >
              Every great idea can be implemented in many ways.
              You want find out the best for yours ?
              come an talk with us we will help you with market
              analyse and plan your succes.
            </Product>
            <Product
              className="sm-col-10 md-col-6 mx-auto p2"
              title="Application project"
            >
              Well planned, user friendly and intuitive in use
              application is the key. No matter if you want
              reproject flow of existing one or you’ve just
              started working on new one - you can trust us
              and our expierience in UI an UX design.
            </Product>
            <Product
              className="sm-col-10 md-col-6 mx-auto p2"
              title="Improving applications"
            >
              You have your application already created
              but you still finding new bugs. A lot of
              people have problems with tidy up their
              existing code. Come to us and let us take a look.
            </Product>
            <Product
              className="sm-col-10 md-col-6 mx-auto p2"
              title="Web applications"
            >
              Complete process of creating websites even
              those with most complicated functionalities.
              Starts with idea and way of comercialize it,
              through graphic design and development.
              Untill giviing product to customres
              (even a bit longer) we are providing the best solutions.
            </Product>
            <Product
              className="sm-col-10 md-col-6 mx-auto p2"
              title="Mobile applications"
            >
              Did you know that more than 90 % people using
              internet regulary using smarphones for it?
              We will give you opportunity to reach them
              in the best possible way - with
              highest quality mobile applications.
            </Product>
            <Product
              className="sm-col-10 md-col-6 mx-auto p2"
              title="Design"
            >
              Logo, suitable fonts, color scheme are the
              basics ways to make your product recognizable.
              But let us do even more - we can create complete
              image of you and your company form the viewpoint
              of customer. Let us take care of your business.
            </Product>
          </div>

        </Element>
      </Section>
    );
  }
}
