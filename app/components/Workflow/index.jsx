import React from 'react';
import { PureComponent } from 'react-pure-render';
import { Element } from 'react-scroll';

import Section from 'Section';
import Headline from 'Headline';
import Work from 'Work';

// import styles from './styles.css';

export default class Workflow extends PureComponent {

  render() {
    return (
      <Section className="flex flex-column justify-between" type="white">
        <Element
          id="workflow"
          name="workflow"
          className="flex flex-column items-center pt3 element"
        >

          <Headline className="my2" index={3} text="Workflow" />

          <div className="mb3">
            <Work
              title="Concept"
              image={require('img/concept.png')}
              faIconId="fa-lightbulb-o"
              gradient="top"
            >
              <h6>
                Come and tell us about your idea.
                No matter if you already have a company which needs
                to be supported by new website or you want to start
                business based on application. You are welcome to come
                and talk with us. Your idea can only be improved!
              </h6>
            </Work>
            <Work
              title="Business consultations"
              image={require('img/business.png')}
              faIconId="fa-bar-chart"
              position="right"
            >
              <h6>
                Are you sure you know your market?
                Have you done competetive analysis?
                We are happy to help you and and prapre
                personalized reports based on your ideas.
                What more ? We help you with adjust you business
                plan to make it as effective as possible.
                Business Strategy is key to maximizing profits
                in the online economy. We know how to design it!
                And guess what? We will share our secrets with you!
              </h6>
            </Work>
            <Work
              title="Projects"
              image={require('img/projects.png')}
              faIconId="fa-sitemap"
            >
              <h6>
                This is the beginning for your journey.
                You have the idea all it needs now is a clear plan.
                Together we can create it! We will make your product
                to be innovative and intuitive for your customers!
                Immpossible you say? Not with US !
                Well planned product and correctly chosen technology
                is the base which give opportunity to works faster
                and more efficient on next steps.
              </h6>
            </Work>
            <Work
              title="Design"
              description="Lorem ipsum dolor sit amet, consectetur adipiscing elit."
              image={require('img/design.png')}
              faIconId="fa-paint-brush"
              position="right"
            >
              <h6>
                Ok, everything is planned but you want to see the effect
                of our work? Here you go! We will keep you updating on
                your project! It feels great to see
                your dreams coming true? Isn’t it?
              </h6>
            </Work>
            <Work
              title="Development"
              image={require('img/development.png')}
              faIconId="fa-code"
            >
              <h6>
                Now it is our turn! Would you trust us and
                give us some time? Yes? Perfect!
                Right now we are transforming your idea into reality.
                When we will finish your product will be working
                and almost ready to go. Be patient!
              </h6>
            </Work>
            <Work
              title="Testing"
              image={require('img/testing.png')}
              faIconId="fa-flask"
              position="right"
            >
              <h6>
                We hope that you will not skip the very
                important part of creating your product
                which are tests and analyze of the first
                feedback. We will tell you how to do it
                and use for your benefits. First feedbacks
                will tell us how to make your idea shine
                even brighter! You think it will be time
                consuming? No worries, we will do it for you!
              </h6>
            </Work>
            <Work
              title="Marketing"
              image={require('img/marketing.png')}
              faIconId="fa-rocket"
              gradient="bottom"
            >
              <h6>
                First impression? It is the most important
                thing for everyone! We will not let you go
                without tools and strategies that will help
                you to build up your brand! We already know
                what is important to build up good relationship
                with customers! Do you know it as well?
              </h6>
            </Work>
          </div>

        </Element>
      </Section>
    );
  }
}
