import React from 'react';
import { PureComponent } from 'react-pure-render';
import { Element, animateScroll } from 'react-scroll';
import cx from 'classnames';

import Section from 'Section';
import Headline from 'Headline';
import Thumbnail from 'Thumbnail';
import Button from 'Button';
import Animate from 'Animate';

import styles from './styles.css';

export default class AboutUs extends PureComponent {

  scrollToBottom() {
    animateScroll.scrollToBottom();
  }

  render() {
    return (
      <Section className={cx('pb3 flex flex-column justify-between', styles.height)} type="white">
        <Element
          id="aboutus"
          name="aboutus"
          className="flex flex-column flex-auto items-center pt3 element"
        >

          <Headline className="my3" index={1} text="Meet us" />

          <Animate
            timeline={{
              topBottom: { width: '100%', opacity: 0 },
              centerCenter: { width: window.innerWidth > 500 ? '50%' : '80%', opacity: 1 },
            }}
            className="flex flex-wrap flex-row flex-auto my2 col-6 sm-col-8"
          >
            <Thumbnail faIconId="fa-child" />
            <Thumbnail faIconId="fa-lightbulb-o" />
            <Thumbnail faIconId="fa-desktop" />
            <Thumbnail faIconId="fa-code" />
            <Thumbnail faIconId="fa-trophy" />
          </Animate>

          <div className="flex flex-row flex-wrap sm-col-12 md-col-10 lg-col-10 my2">
            <Animate
              timeline={{
                topBottom: { scale: 0.7 },
                centerCenter: { scale: 1, ease: 'easeOutCubic' },
              }}
              className="lg-col-4"
            >
              <p className={cx('col-12 bold sm-center lg-right-align p2', styles.boldText)}>
                appright.io is a modern company full of open-minded people
                who are up-to-date with newest programming knowledge about
                every kind of software. We are offering you our help on
                every step of turning your idea into reality.
              </p>
            </Animate>
            <Animate
              timeline={{
                topBottom: { scale: 0.7 },
                centerCenter: { scale: 1, ease: 'easeOutCubic' },
              }}
              className="lg-col-4"
            >
              <p className="col-12 p2 sm-center">
                It doesn’t matter if you want create simple website
                or advanced application. Maybe you just want to consult
                your idea or work is partially done and you are looking
                for someone who will pimp it ? Together we can even give
                a new life to everyday stuff and make your house smart.
                Anything you need or can think of! Look at our services and
                find out more about technologies we are using!
              </p>
            </Animate>
            <Animate
              timeline={{
                topBottom: { scale: 0.7 },
                centerCenter: { scale: 1, ease: 'easeOutCubic' },
              }}
              className="lg-col-4"
            >
              <p className="col-12 p2 sm-center">
                 Starting with business plan, choosing the best strategy,
                 estimating costs throught materializing your idea if it comes
                 to visual  and technological site ending with building your
                 brand, marketing and customer service. We will be with
                 you on whole way to succes.
              </p>
            </Animate>
          </div>

          <Button className="mb2" text="Let's work!" onClick={this.scrollToBottom} />

        </Element>
      </Section>
    );
  }
}
