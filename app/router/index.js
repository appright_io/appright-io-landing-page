import React from 'react';
import { Router, Route, Redirect, browserHistory } from 'react-router';

// Routes
import Home from '../containers/Home.jsx';

export default (
  <Router history={browserHistory}>
    <Route component={Home} path="/" />
    <Redirect from="*" to="/" />
  </Router>
);
