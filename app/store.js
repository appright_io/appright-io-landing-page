// Redux
import { createStore, compose, applyMiddleware } from 'redux';

// Reducer
import rootReducer from './reducers';

// Sagas
import sagaMiddleware from 'redux-saga';
import sagas from './sagas';

export default createStore(
  rootReducer,
  compose(
    applyMiddleware(sagaMiddleware(...sagas))
  )
);
