export default function startsWith(string, query) {
  return string.toLowerCase().indexOf(query) >= 0;
}
