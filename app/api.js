import { api } from './config';

// Private function extracting response
function status(response) {
  const parseBody = response.json();

  return parseBody
    .catch(() => Promise.reject({
      global: 'There was an network error, try again',
    }))
    .then(body => new Promise((resolve, reject) => {
      if (response.status >= 200 && response.status < 300) {
        return resolve(body, response);
      }
      return reject({
        ...body.errors,
        statusCode: response.status,
      }, response);
    }));
}

/**
 * Proxy to window.fetch
 *
 * @description
 * Automatically adds common headers
 * and normilise response
 *
 * @param {string} url to fetch
 * @param {Object} opts - options
 * @returns {Function} status
 */
export function fetch(url, opts = {}) {
  const resultURL = `${api}/${url}`;
  const resultOptions = opts;

  resultOptions.headers = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  };

  if (opts.body) {
    resultOptions.body = JSON.stringify(opts.body);
  }
  return window
    .fetch(resultURL, resultOptions)
    .then(status);
}
