const devConfig = {
  api: 'http://localhost:8000',
  env: 'development',
};

const prodConfig = {
  api: 'https://appright.herokuapp.com',
  env: 'production',
};

module.exports = process.env.NODE_ENV === 'development'
  ? devConfig
  : { ...devConfig, ...prodConfig };
