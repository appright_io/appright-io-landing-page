import { createSelector } from 'reselect';

const menu =
  [
    { id: 'aboutus', content: 'Meet us' },
    { id: 'products', content: 'Products' },
    { id: 'workflow', content: 'Workflow' },
    { id: 'technology', content: 'Technology' },
    { id: 'contact', content: 'Contact' },
  ];

const appStateSelector = (state) => state.get('app');

export const applicationResponseSelector = createSelector(
  appStateSelector,
  (app) => app.get('response')
);

export const activeIndexSelector = createSelector(
  appStateSelector,
  (app) => app.get('activeIndex')
);

export const contactFormSelector = createSelector(
  appStateSelector,
  (app) => app.get('contactForm')
);

export const isMenuOpenSelector = createSelector(
  appStateSelector,
  (app) => app.get('isMenuOpen')
);

export const menuSelector = () => menu;
